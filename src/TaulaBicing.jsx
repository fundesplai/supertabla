import React from "react";
import { Table } from "reactstrap";
import styled from 'styled-components';


const ClicableTh = styled.th`
    cursor: pointer;
    :hover {
        text-decoration: underline;
        color: red;
    }
`;

export default (props) => {

  const filas = props.datos.map((el) => (
    <tr key={el.id}>
      {props.columnas.map((col, idx) => <td key={idx}>{el[col['nom']]}</td>)}
    </tr>
  ));

  return (
    <Table striped>
      <thead>
        <tr>
          {props.columnas.map((el, idx) => <td key={idx}>{el['titol']}</td>)}
        </tr>
      
      </thead>
      <tbody>{filas}</tbody>
      <tfoot></tfoot>
    </Table>
  );
};
