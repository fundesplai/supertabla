import React, { useState, useEffect } from "react";
import { Container, Col, Row } from "reactstrap";
import TaulaBicing from "./TaulaBicing.jsx";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";

const DatosEstacion = (props) => (
  <>
    <h5>{props.estacion.name}</h5>
    <table>
      <tbody>
        <tr>
          <td>Slots lliures</td>
          <th>{props.estacion.empty_slots}</th>
        </tr>
        <tr>
          <td>Bicis disponibles</td>
          <th>{props.estacion.free_bikes}</th>
        </tr>
      </tbody>
    </table>
  </>
);

export default () => {
  // datos que se muestran en la tabla, inicialmente son todas las estaciones
  const [estacionsVisibles, setEstacionsVisibles] = useState([]);

  // HOOKS--------------------------------------------
  useEffect(() => {
    fetch("https://api.citybik.es/v2/networks/bicing")
      .then((dades) => dades.json())
      .then((dades) => setEstacionsVisibles(dades.network.stations))
      .catch((err) => console.log(err));

    console.log("fetch lanzado");
  }, []); // similar a componentDidMount

  const columnes = [
    {
      nom: "name",
      titol: "estació",
    },
    {
      nom: "free_bikes",
      titol: "bicis disponibles",
    },
    {
      nom: "empty_slots",
      titol: "slots lliures",
    },
  ];

  let mapa = <h3>Cargando mapa...</h3>;
  if (estacionsVisibles.length > 0) {
    mapa = (
      <Map
        style={{ height: "600px", width: "100%" }}
        zoomControl={true}
        doubleClickZoom={true}
        scrollWheelZoom={true}
        dragging={true}
        animate={true}
        center={[estacionsVisibles[0].latitude, estacionsVisibles[0].longitude]}
        zoom={20}
      >
        {estacionsVisibles.map((el, idx) => (
          <Marker key={idx} position={[el.latitude, el.longitude]}>
            <Popup><DatosEstacion estacion={el} /> </Popup>
          </Marker>
        ))}
        <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
      </Map>
    );
  }

  return (
    <>
      <Container>
        <h1>Estacions</h1>
        <Row>
          <Col className="col-6 col-lg-4">
            <TaulaBicing datos={estacionsVisibles} columnas={columnes} />
          </Col>
          <Col xs="6" lg="8">
            {mapa}
          </Col>
        </Row>
      </Container>
    </>
  );
};
