import React from "react";
import { Table, Button } from "reactstrap";
import styled from 'styled-components';


const ClicableTh = styled.th`
    cursor: pointer;
    :hover {
        text-decoration: underline;
        color: red;
    }
`;

export default (props) => {
  const nombreCol = "model";

  const filas = props.datos.map((el) => (
    <tr key={el.id}>
      <td>{el.id}</td>
      <td>{el.marca}</td>
      <td>{el[nombreCol]}</td>
      <td>
        <Button color="danger" onClick={() => props.borra(el.id)}>
          Borrar
        </Button>
        {" "}
        <Button color="success" onClick={() => props.muestra(el.id)}>
          Mostrar
        </Button>
      </td>
    </tr>
  ));

  return (
    <Table striped>
      <thead>
        <tr>
          <ClicableTh onClick={() => props.ordena('id', true)}>#</ClicableTh>
          <ClicableTh onClick={() => props.ordena('marca')}>Marca</ClicableTh>
          <ClicableTh onClick={() => props.ordena('model')}>Model</ClicableTh>
          <th></th>
        </tr>
        <tr>
          <td></td>
          <td>
            <input onChange={(e) => props.filtra(e.target.value, "marca")} />
          </td>
          <td>
            <input onChange={(e) => props.filtra(e.target.value, "model")} />
          </td>
          <td></td>
        </tr>
      </thead>
      <tbody>{filas}</tbody>
      <tfoot></tfoot>
    </Table>
  );
};
