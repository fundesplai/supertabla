import React, { useState, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container } from 'reactstrap';
import Taula from './Taula.jsx';


import MOTOS from './datos.json';

// de buen principio añadimos id a MOTOS
const MOTOS_CON_ID = MOTOS.map( (el,idx) => {
  el.id = idx + 1;
  return el;
});


export default () => {


  // HOOKS-------------------------------------------- 

  // datos que se muestran en la tabla, inicialmente son todas las motos
  const [motosVisibles, setMotosVisibles] = useState(MOTOS_CON_ID);
  // moto "actual", se mostrará en el modal
  const [laMoto, setLaMoto] = useState(MOTOS[0]);
  // filtros de modelo y marca (texto parcial que buscará en el dato)
  const [modelFilter, setModelFilter] = useState('');
  const [marcaFilter, setMarcaFilter] = useState('');
  // determina si la ventana modal es visible o no
  const [modalVisible, setModalVisible] = useState(false);
  // determina sentido asc/desc de la ordenación, cada llamada a ordenar cambia el sentido
  const [ascendente, setAscendente] = useState(false);


 
  // filtra los datos cuando se produce un cambio en modelFilter o marcaFilter
  useEffect(() => {
    let filtrados = [...MOTOS_CON_ID]; // partimos de todos los datos
    if (modelFilter) {
      filtrados = filtrados.filter(el => el.model.toLowerCase().includes(modelFilter.toLowerCase()))
    }
    if (marcaFilter) {
      filtrados = filtrados.filter(el => el.marca.toLowerCase().includes(marcaFilter.toLowerCase()))
    }
    setMotosVisibles(filtrados);
  }, [modelFilter, marcaFilter])


  // FUNCIONES -----------------------

   // cambia la visibilidad de la ventana modal
   const cambiaVisibildadModal = () => setModalVisible(!modalVisible);

  // elimina moto con id=x recibido... cuidado! al cambiar el filtro reaparecerá, por qué?
  const borraItem = (x) => {
    console.log("borrar ", x);
    setMotosVisibles(motosVisibles.filter(el => el.id !== x));
  }

  // muestra moto con id=x
  const muestraItem = (x) => {
    setLaMoto(motosVisibles.find(el => el.id === x));
    cambiaVisibildadModal();
  }

  // establece criterio de filtro, llamado al cambiar el campo input en la cabecera de la tabla
  // recibe el nuevo valor y el campo que se está filtrando (model o marca)
  const filtra = (valor, campo) => {
    if (campo === 'model') {
      setModelFilter(valor);
    }
    if (campo === 'marca') {
      setMarcaFilter(valor);
    }
  }

  // ordena segun el campo recibido y el estado del state 'ascendente'
  const ordena = (camp, numeric=false) => {
    const temp = [...motosVisibles];
    if (numeric){
      if (ascendente) {
        temp.sort((a, b) => (a[camp] > b[camp]) ? 1 : -1);
      } else {
        temp.sort((a, b) => (a[camp] < b[camp]) ? 1 : -1);
      }
    } else {
      if (ascendente) {
        temp.sort((a, b) => (a[camp].toLowerCase() > b[camp].toLowerCase()) ? 1 : -1);
      } else {
        temp.sort((a, b) => (a[camp].toLowerCase() < b[camp].toLowerCase()) ? 1 : -1);
      }
    }

    setMotosVisibles(temp);
    setAscendente(!ascendente);
  }

  return (
    <>
      <Container>
        <h1>Motos</h1>

        <Taula datos={motosVisibles} borra={borraItem} muestra={muestraItem} filtra={filtra} ordena={ordena} />

        <Modal isOpen={modalVisible} toggle={cambiaVisibildadModal} >
          <ModalHeader toggle={cambiaVisibildadModal}>{laMoto.marca} <strong>{laMoto.model}</strong></ModalHeader>
          <ModalBody>
            {laMoto.cc} cc <br />
            {laMoto.km} km <br />
            <strong>{laMoto.eur} EUR </strong><br />
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={cambiaVisibildadModal}>Ok</Button>
          </ModalFooter>
        </Modal>
      </Container>


    </>
  );
}

